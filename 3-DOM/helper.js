function formateo(time) {
  if (time < 10) {
    return '0' + time;
  }
}

export { formateo };
