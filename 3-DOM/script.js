// Seleccionamos el body que albergará nuestro magnífico reloj.
const body = document.querySelector('body');

// Creamos las variables horas,minutos,segundos
function formateo(time) {
  if (time < 10) {
    return `0${time}`;
  } else {
    return time;
  }
}

// Modificamos las propiedades del body para que el reloj esté centrado.
body.setAttribute(
  'style',
  'height : 100vh; display:flex ; justify-content:center; align-items:center; font-size : 5rem'
);
function decirLaHora() {
  const fecha = new Date();

  const hora = formateo(fecha.getHours());
  const minuto = formateo(fecha.getMinutes());
  const segundo = formateo(fecha.getSeconds());

  body.innerHTML = `
<div >  ${hora} : ${minuto} : ${segundo} </div>
`;
}

setInterval(() => {
  decirLaHora();
}, 1000);

decirLaHora();
