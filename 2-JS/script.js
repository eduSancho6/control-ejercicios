'use strict';

// puntuaciones
const puntuaciones = [
  {
    equipo: 'Toros Negros',
    puntos: [1, 3, 4, 2, 10, 8],
  },
  {
    equipo: 'Amanecer Dorado',
    puntos: [8, 5, 2, 4, 7, 5, 3],
  },
  {
    equipo: 'Águilas Plateadas',
    puntos: [5, 8, 3, 2, 5, 3],
  },
  {
    equipo: 'Leones Carmesí',
    puntos: [5, 4, 3, 1, 2, 3, 4],
  },
  {
    equipo: 'Rosas Azules',
    puntos: [2, 1, 3, 1, 4, 3, 4],
  },
  {
    equipo: 'Mantis Verdes',
    puntos: [1, 4, 5, 1, 3],
  },
  {
    equipo: 'Ciervos Celestes',
    puntos: [3, 5, 1, 1],
  },
  {
    equipo: 'Pavos Reales Coral',
    puntos: [2, 3, 2, 1, 4, 3],
  },
  {
    equipo: 'Orcas Moradas',
    puntos: [2, 3, 3, 4],
  },
];

function descubrirEquipos(array) {
  let arraySumada = [];

  for (const punts of array) {
    let suma = 0;
    for (let i = 0; i < punts.puntos.length; i++) {
      suma += punts.puntos[i];
    }
    arraySumada.push(suma);
  }

  let numeroMayor = 0;
  let equipoGan = '';

  let numeroMenor = 0;
  let equipoPer = '';

  for (let i = 0; i < arraySumada.length; i++) {
    if (arraySumada[i] > numeroMayor) {
      numeroMayor = arraySumada[i];
      equipoGan = array[i].equipo;
    }
  }
  for (let i = 0; i < arraySumada.length; i++) {
    if (arraySumada[i] < numeroMenor || numeroMenor === 0) {
      numeroMenor = arraySumada[i];
      equipoPer = array[i].equipo;
    }
  }
  console.log(`Con una puntuación de ${numeroMayor} , el equipo que ha conseguido la victoria eeeeeees....
  ${equipoGan} !!!!!! ENHORABUENA!!!`);
  console.log(`Con una puntuación de ${numeroMenor} , el equipo que ha sido humillantemente derrotado eeeeeees....
  ${equipoPer} !!!!!! AL CARRER!!!`);
}
descubrirEquipos(puntuaciones);
